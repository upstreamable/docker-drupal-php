# Versioning

This image is a derivative of [wodby/drupal-php](https://github.com/wodby/drupal-php)
To release this image as a composer package this pattern is followed:

* First comes the PHP major version. As many digits as needed.
* 1 Digit for PHP minor version.
* 2 Digits for the major version from wodby.
* Dot separator.
* 2 Digits for the minor version from wodby.
* 1 Digit for the patch version from wodby.
* Dot separator
* 1 Digit for the changes in this image.

For example the tag for this image:
8004.293.0
Corresponds with
8.0-4.29.3
