#!/usr/bin/env bash

set -e

if  [ -n "${DRUPAL_WAIT_FOR_DB}" ]; then
  command="mysql --protocol TCP -h${DRUPAL_DATABASE_HOST} -u${DRUPAL_DATABASE_USERNAME} -p${DRUPAL_DATABASE_PASSWORD} -e \"show databases;\""
  host=${DRUPAL_DATABASE_HOST}
  max_try="10"
  wait_seconds="1"
  delay_seconds="0"
  service="mariadb"
  wait_for "${command}" ${service} ${host} ${max_try} ${wait_seconds} ${delay_seconds}
fi
