#!/usr/bin/env bash

set -e

# Force TCP and output just the number of tables without decorations (--silent).
command="mysql --protocol TCP --skip-column-names --silent --host=${MYSQL_HOST} --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --execute  \"SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${MYSQL_DATABASE}';\" ${MYSQL_DATABASE}"

if [ -n "${DRUPAL_COMMAND_ON_EMPTY_DB}" ] && [ "$(eval "$command")" = "0" ]; then
  eval "${DRUPAL_COMMAND_ON_EMPTY_DB}"
fi
