#!/usr/bin/env bash

set -e

if [ -n "${DRUPAL_INIT_COMMAND}" ]; then
  eval "${DRUPAL_INIT_COMMAND}"
fi
