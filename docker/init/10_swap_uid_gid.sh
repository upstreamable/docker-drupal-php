#!/usr/bin/env bash

set -e

if [ -n "${SWAP_WWWDATA_UIDGID_FOR_HOST}" ] && [ -n "${HOST_UID}" ] && [ -n "${HOST_GID}" ]; then
  # Delete www-data user
  deluser www-data || true
  # Delete the user occuping the uid we want to use.
  deluser $(getent passwd "$HOST_UID" | cut -d: -f1) || true
  addgroup -g ${HOST_GID} www-data
  adduser -D -h /var/www/html -s /bin/bash -u ${HOST_UID} -G www-data www-data

  # Init container again as the uids might have changed.
  init_container
fi

chown -R www-data:www-data /var/www/html
